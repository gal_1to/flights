var API_KEY = 'YOUR_API_KEY';
var URL_API_QPX = 'https://www.googleapis.com/qpxExpress/v1/trips/search';
var KIND_DATA_QPX = 'qpxexpress#sliceInput';

$(document).on('ready', initComponent());


function initComponent(){
	// non stops defaults
	var nonStopsCities = ['PHX', 'LAS', 'LAX', 'SEA', 'SAN', 'SNA', 'PDX'];

	// HIDE FLIHGT LIST
	$('#results-data').hide();
	// ADDED LISTENER FOR CLICK TO BTN
	$('#btnCheckFlights').on('click', function(e){
		// removing values previus search
		$("#flihgt-list").empty();


		var isDestinationNonStop = $.inArray($('#destination_city').val(), nonStopsCities) > -1;
		var nonStopAvailable = isDestinationNonStop ? 0 : 3;

		var jsonParams = {
			'kind': KIND_DATA_QPX,
			'origin': 'SFO',
			'destination': $('#destination_city').val(),
			'date': $('#departure_date').val(),
			'maxStops': nonStopAvailable
		}


		callGoogleFlights(jsonParams);

	});


	





	/*var stringJson = '{"kind":"qpxexpress#data","airport":[{"kind":"qpxexpress#airportData","code":"EWR","city":"EWR","name":"Newark Liberty International"},{"kind":"qpxexpress#airportData","code":"SFO","city":"SFO","name":"San Francisco International"}],"city":[{"kind":"qpxexpress#cityData","code":"EWR","name":"Newark"},{"kind":"qpxexpress#cityData","code":"SFO","name":"San Francisco"}],"aircraft":[{"kind":"qpxexpress#aircraftData","code":"77W","name":"Boeing 777"}],"tax":[{"kind":"qpxexpress#taxData","id":"ZP","name":"US Flight Segment Tax"},{"kind":"qpxexpress#taxData","id":"AY_001","name":"US September 11th Security Fee"},{"kind":"qpxexpress#taxData","id":"US_001","name":"US Transportation Tax"},{"kind":"qpxexpress#taxData","id":"XF","name":"US Passenger Facility Charge"}],"carrier":[{"kind":"qpxexpress#carrierData","code":"UA","name":"United Airlines, Inc."}]}';

	printResults(JSON.parse(stringJson));*/
}


function callGoogleFlights(params){
	// CUSTOM REQUEST
	var requestParams = {
		"request": {
	        "passengers": {
	            "adultCount": "1"
	        },
	        "slice": [
	        	params
	        ],
	        "solutions": "1"
	    }
	}

	$.ajax({
        url: URL_API_QPX + '?key=' + API_KEY,
        async:true,
        beforeSend: function(object){
            console.log("Before sending");
        },
        complete: function(object, success){
            if(success=="success"){
                console.log("Request success");
            }
        },
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(requestParams),
        dataType: "json",
        error: function(request, error, obj){
            console.log("Error on request: " + request.responseText);
        },
        global: true,
        ifModified: false,
        processData:true,
        success: function(response){
        	$('#results-data').show();
            printResults(response['trips']['data']);
        },
        type: "POST"
	});
}


function printResults(data){
	// ADDING CARRIER NAME
    $('#flihgt-list').append('<li class="collection-header"><h4>Carrier</h4></li>');
	$.each(data.carrier, function(key, value) {
	    $('#flihgt-list').append('<li><a href="https://www.google.com.mx/#q='+value.name+'" target="_Blank">'+value.name+'</a></li>');
	});

	// ADDING AIRPORT STOP
	$('#flihgt-list').append('<li class="collection-header"><h4>Airport</h4></li>');
	$.each(data.airport, function(key, value) {
	    $('#flihgt-list').append('<li>'+ value.name + ' ' + value.code +'</li>');
	});

	$('#flihgt-list').append('<li class="collection-header"><h4>City</h4></li>');
	$.each(data.city, function(key, value) {
	    $('#flihgt-list').append('<li>'+ value.name + ' ' + value.code +'</li>');
	});
}

